﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Contacts
    {
        public string surname;
        public string name;
        public string patronymic;
        public string number;
        public string country;
        public string birthday;
        public string organization;
        public string post;
        public string other;

        public Contacts(string surname, string name, string patronymic, string number, string country, string birthday, string organization, string post, string other)
        {
            this.surname = surname;
            this.name = name;
            this.patronymic = patronymic;
            this.number = number;
            this.country = country;
            this.birthday = birthday;
            this.organization = organization;
            this.post = post;
            this.other = other;
        }
    }
    class PhoneBook
    {
        List<Contacts> contacts;

        public PhoneBook()
        {
            contacts = new List<Contacts>();
        }

        public void add(string surname, string name, string patronymic, string number, string country, string birthday, string organization, string post, string other)
        {
            Contacts date = new Contacts(surname, name, patronymic, number, country, birthday, organization, post, other);
            contacts.Add(date);
        }

        public bool remove(string name)       
        {
            Contacts date = find(name);

            if (date != null)
            {
                contacts.Remove(date);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void list(Action<Contacts> action)  
        {
            contacts.ForEach(action);
        }

        public bool isEmpty()
        {
            return (contacts.Count == 0);
        }
        public bool find2(string name)
        {
            Contacts date = contacts.Find(
              delegate (Contacts a)   
              {
                  return a.name == name;
              }
            );
            Contacts date1 = contacts.FindLast(
             delegate (Contacts a)
             {
                 return a.name == name;
             }
           );
            if (date1 == date)
                return true;
            else
                return false;
        }

        public Contacts find(string name)
        {
            Contacts date = contacts.Find(
              delegate (Contacts a)  
              {
                  return a.name == name;
              }
            );
            return date;
        }
        public Contacts findwiths(string name, string surname)
        {
            Contacts date = contacts.Find(
              delegate (Contacts a) 
              {
                  return (a.name == name && a.surname == surname);
              }
            );
            return date;
        }
    }
    class PhonePrompt
    {
        PhoneBook book;

        public PhonePrompt()
        {
            book = new PhoneBook();
        }

        static void Main(string[] args)
        {
            string selection = "";
            PhonePrompt prompt = new PhonePrompt();

            prompt.displayMenu();
            while (!selection.ToUpper().Equals("Q"))
            {
                Console.WriteLine("Selection: ");
                selection = Console.ReadLine();
                prompt.performAction(selection);
            }
        }

        void displayMenu()
        {
            Console.WriteLine("Main Menu");
            Console.WriteLine("=========");
            Console.WriteLine("A - Add an Contact");
            Console.WriteLine("D - Delete an Contact");
            Console.WriteLine("E - Edit an Contact");
            Console.WriteLine("L - List All Contacts");
            Console.WriteLine("C - List Contact");
            Console.WriteLine("Q - Quit");
        }

        void performAction(string selection)
        {
            string surname = "";
            string name = "";
            string patronymic = "";
            string number = "";
            string country = "";
            string birthday = "";
            string organization = "";
            string post = "";
            string other = "";

            switch (selection.ToUpper())
            {
                case "A":
                    Console.WriteLine("Enter Surname: ");
                    surname = Console.ReadLine();
                    while (surname == "")
                    {
                        Console.WriteLine("Warning!!! Enter Surname!");
                        surname = Console.ReadLine();
                    }
                    Console.WriteLine("Enter Name: ");
                    name = Console.ReadLine();
                    while (name == "")
                    {
                        Console.WriteLine("Warning!!! Enter Name!");
                        name = Console.ReadLine();
                    }
                    Console.WriteLine("Enter Patronymic: ");
                    patronymic = Console.ReadLine();
                    Console.WriteLine("Enter Number: ");
                    long result;
                    number = Console.ReadLine();
                    while (number == "")
                    {
                        Console.WriteLine("Warning!!! Enter Number!");
                        number = Console.ReadLine();
                    }
                    result = Convert.ToInt64(number);
                    Console.WriteLine("Enter Country: ");
                    country = Console.ReadLine();
                    while (country == "")
                    {
                        Console.WriteLine("Warning!!! Enter Country!");
                        country = Console.ReadLine();
                    }
                    Console.WriteLine("Enter Birthday: ");
                    birthday = Console.ReadLine();
                    Console.WriteLine("Enter Organization: ");
                    organization = Console.ReadLine();
                    Console.WriteLine("Enter Post: ");
                    post = Console.ReadLine();
                    Console.WriteLine("Enter Other: ");
                    other = Console.ReadLine();
                    book.add(surname, name, patronymic, number, country, birthday, organization, post, other);
                    Console.WriteLine("Contact successfully added!");
                    break;
                case "D":
                    Console.WriteLine("Enter Name to Delete: ");
                    name = Console.ReadLine();
                    if (book.remove(name))
                    {
                        Console.WriteLine("Contact successfully removed");
                    }
                    else
                    {
                        Console.WriteLine("Contact for {0} could not be found.", name);
                        Console.ReadKey();
                    }
                    break;
                case "L":
                    if (book.isEmpty())
                    {
                        Console.WriteLine("There are no entries.");
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("Contacts:");
                        book.list(
                          delegate (Contacts a)
                          {
                              Console.WriteLine("{0} - {1} - {2}", a.surname, a.name, a.number);
                          }
                        );
                    }
                    break;
                case "E":
                    Console.WriteLine("Enter Name to Edit: ");
                    name = Console.ReadLine();
                    Contacts date;
                    if (book.find2(name))
                        date = book.find(name);
                    else
                    {
                        Console.WriteLine("Enter Surname to Edit: ");
                        surname = Console.ReadLine();
                        date = book.findwiths(name, surname);
                    }

                    if (date == null)
                    {
                        Console.WriteLine("Contact for {0} count not be found.", name);
                        Console.ReadKey();
                    }
                    else
                    {
                        string edition = "";
                        PhonePrompt edit = new PhonePrompt();
                        Console.WriteLine("Enter what you edit: ");
                        edition = Console.ReadLine();
                        edit.performAction(edition);
                        switch (edition)
                        {
                            case "surname":
                                Console.WriteLine("Enter new Surname: ");
                                date.surname = Console.ReadLine();
                                Console.WriteLine("Contact updated for {0}", name);
                                break;
                            case "name":
                                Console.WriteLine("Enter new Name: ");
                                date.name = Console.ReadLine();
                                Console.WriteLine("Contact updated for {0}", name);
                                break;
                            case "patronymic":
                                Console.WriteLine("Enter new Patronymic: ");
                                date.patronymic = Console.ReadLine();
                                Console.WriteLine("Contact updated for {0}", name);
                                break;
                            case "number":
                                Console.WriteLine("Enter new Number: ");
                                date.number = Console.ReadLine();
                                Console.WriteLine("Contact updated for {0}", name);
                                break;
                            case "country":
                                Console.WriteLine("Enter new Country: ");
                                date.country = Console.ReadLine();
                                Console.WriteLine("Contact updated for {0}", name);
                                break;
                            case "birthday":
                                Console.WriteLine("Enter new Birthday: ");
                                date.birthday = Console.ReadLine();
                                Console.WriteLine("Contact updated for {0}", name);
                                break;
                            case "organization":
                                Console.WriteLine("Enter new Organization: ");
                                date.organization = Console.ReadLine();
                                Console.WriteLine("Contact updated for {0}", name);
                                break;
                            case "post":
                                Console.WriteLine("Enter new Post: ");
                                date.post = Console.ReadLine();
                                Console.WriteLine("Contact updated for {0}", name);
                                break;
                            case "other":
                                Console.WriteLine("Enter new Other: ");
                                date.other = Console.ReadLine();
                                Console.WriteLine("Contact updated for {0}", name);
                                break;
                        }
                    }
                    break;
                case "C":
                    Console.WriteLine("Enter Name to Found: ");
                    name = Console.ReadLine();
                    Contacts find;
                    if (book.find2(name))
                        find = book.find(name);
                    else
                    {
                        Console.WriteLine("Enter Surname to Found: ");
                        surname = Console.ReadLine();
                        find = book.findwiths(name, surname);
                    }
                    if (find == null)
                    {
                        Console.WriteLine("Contact for {0} count not be found.", name);
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("Contact:");
                        Console.WriteLine("Фамилия:{0}, Имя:{1}, Отчество:{2}, Номер телефона:{3}, Страна:{4}, Дата рождения:{5}, Организация:{6}, Должность:{7}, Другое:{8}"
                            , find.surname, find.name, find.patronymic, find.number, find.country, find.birthday, find.organization, find.post, find.other);
                    }
                    break;
            }
        }
    }
}